import MDeditor from '../../pages/markdown_editor';

const titleSelector = "[aria-label=title]";

describe('MarkdownEditor.cy.js', () => {
  it('playground', () => {
    cy.mount(<MDeditor />)
    cy.get(titleSelector).should("have.text", "Markdown Editor");
  })
})